package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRoleRepository extends JpaRepository<AppRole,Long> {
    AppRole findByRoleName(String name);
}
