package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepository extends JpaRepository<AppUser,Long> {

    AppUser findByUsername(String  username);
}
