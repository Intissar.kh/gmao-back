package org.sid.gmao_back.repositories;


import org.sid.gmao_back.entities.EtatMaintenance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface EtatMaintenanceRepository extends JpaRepository<EtatMaintenance,Long>{
    @RestResource(path = "/byDesignation")
    public List<EtatMaintenance> findByDescriptionContains(@Param("mc") String des);
     @RestResource(path = "/byDesignationPage")
    public Page<EtatMaintenance> findByDescriptionContains(@Param("mc") String des, Pageable pageable);

}
