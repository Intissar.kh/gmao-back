package org.sid.gmao_back;

import org.sid.gmao_back.entities.AppRole;
import org.sid.gmao_back.entities.AppUser;
import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.Task;
import org.sid.gmao_back.repositories.EtatMaintenanceRepository;
import org.sid.gmao_back.repositories.TaskRepository;
import org.sid.gmao_back.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.stream.Stream;

@SpringBootApplication
public class GmaoBackApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(GmaoBackApplication.class, args);
    }

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private EtatMaintenanceRepository etatMaintenanceRepository;
    @Bean
    public BCryptPasswordEncoder getBCPE(){
        return new BCryptPasswordEncoder();
    }
    @Override
    public void run(String... args) throws Exception {
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court"));
        Stream.of("fait").forEach(e->{
            etatMaintenanceRepository.save(new EtatMaintenance(null,"ARS123",e));
        });
        etatMaintenanceRepository.findAll().forEach(e->{
            System.out.println(e.getDescription());
        });
        accountService.saveUser(new AppUser(null,"user","1234",null));
        accountService.saveUser(new AppUser(null,"admin","1234",null));
        accountService.saveRole(new AppRole(null,"USER"));
        accountService.saveRole(new AppRole(null,"ADMIN"));
        accountService.addRoleToUser("admin","ADMIN");
        //accountService.addRoleToUser("admin","USER");
        accountService.addRoleToUser("user","USER");
        Stream.of("T1","T2","T3").forEach(t->{
            taskRepository.save(new Task(null,t));
        });
        taskRepository.findAll().forEach(t->{
            System.out.println(t.getTaskName());
        });
    }
}
