package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.AppRole;
import org.sid.gmao_back.entities.AppUser;

public interface AccountService {

    public AppUser saveUser(AppUser user);
    public AppRole saveRole(AppRole role);
    public void addRoleToUser(String username,String role);
    public AppUser findUserByUsername(String username);
}
