package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.exceptions.AttributesNotFound;
import org.sid.gmao_back.exceptions.ErrorType;
import org.sid.gmao_back.exceptions.IdNotFound;

import java.util.List;

public interface EtatMaintenanceService {

    List<EtatMaintenance> findAll();
    EtatMaintenance findById(Long id) throws IdNotFound;
    EtatMaintenance update(Long id,EtatMaintenance etatMaintenance);
    EtatMaintenance save(EtatMaintenance etatMaintenance);
    void delete(long id);



}
